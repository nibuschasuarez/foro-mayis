import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RecetarioComponent } from './components/recetario/recetario.component';
import { AgregarRecetaComponent} from './components/recetario/agregar-receta/agregar-receta.component';
import { VerRecetaComponent } from './components/recetario/ver-receta/ver-receta.component';
import { SharedModule } from './components/shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    RecetarioComponent,
    AgregarRecetaComponent,
    VerRecetaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
